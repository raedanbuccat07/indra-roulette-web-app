import "./App.css";
import { useState, useEffect, Fragment, useCallback, useRef, forwardRef } from "react";
import Swal from "sweetalert2/dist/sweetalert2.all.min.js";
import { Howl } from "howler";
import RouletteSound from "./audio/roulette.mp3";
import Jackpot from "./audio/Jackpot.mp3";
import ReactCanvasConfetti from "react-canvas-confetti";
import * as xlsx from "xlsx";
import { Scrollbars } from "react-custom-scrollbars-2";
import { RouletteWheel } from 'react-casino-roulette';

import Sparkle from 'react-sparkle'
import { Animate } from "react-simple-animate";
import logoRoulette from "./images/logo.png";
import backgroundDefault from './images/bg.jpg';
import defaultBackground from './images/bg.jpg';

//material ui
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Unstable_Grid2';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

import Dialog from '@mui/material/Dialog';
import ListItemText from '@mui/material/ListItemText';
import ListItem from '@mui/material/ListItem';
import List from '@mui/material/List';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Slide from '@mui/material/Slide';

const canvasStyles = {
  position: "fixed",
  pointerEvents: "none",
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
};

function App() {
  const [randomizerData, setRandomizerData] = useState([]);
  const [randomizerUsed, setRandomizerUsed] = useState([]);
  const [randomizerDataOriginal, setRandomizerDataOriginal] = useState([]);
  const [randomizerDisplay, setRandomizerDisplay] = useState([]);
  const [excludedNumber, setExcludedNumber] = useState([]);
  const [backgroundImage, setBackgroundImage] = useState("");
  const [arrayKeyNames, setArrayKeyNames] = useState([]);
  const [firstData, setFirstData] = useState();
  const [secondData, setSecondData] = useState();

  //roulette
  const [doneActiveSpin, setDoneActiveSpin] = useState(false);
  const [showWinnerName, setShowWinnerName] = useState(false);
  const [doneWheelSpin, setDoneWheelSpin] = useState(false);
  const [displayFirstNumber, setDisplayFirstNumber] = useState();
  const [displayAllId, setDisplayAllId] = useState();
  const [backgroundBlur, setBackgroundBlur] = useState(0);


  //wheel
  const [start, setStart] = useState(false);
  const [winningBet, setWinningBet] = useState('-1');

  //confetti
  const refAnimationInstance = useRef(null);

  const getInstance = useCallback((instance) => {
    refAnimationInstance.current = instance;
  }, []);

  const makeShot = useCallback((particleRatio, opts) => {
    refAnimationInstance.current &&
      refAnimationInstance.current({
        ...opts,
        origin: { y: 0.7 },
        particleCount: Math.floor(200 * particleRatio),
      });
  }, []);

  const fire = useCallback(() => {
    makeShot(0.25, {
      spread: 26,
      startVelocity: 55,
    });

    makeShot(0.2, {
      spread: 60,
    });

    makeShot(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  }, [makeShot]);

  // Employee Randomizer
  async function dataRandomizer(randomInt) {
    setRandomizerDisplay(randomizerData[randomInt]);
    setRandomizerUsed([...randomizerUsed, randomizerData[randomInt]]);
    var arrayRandom = randomizerData[randomInt];
    var randomWinner = arrayRandom[firstData]?.toString();
    var placeHolder = "777777";
    
    if (randomWinner === undefined) {
      setDisplayFirstNumber(placeHolder.charAt(0));
      setDisplayAllId(placeHolder);
      setWinningBet(placeHolder.charAt(0).toString());
    } else {
      setDisplayFirstNumber(randomWinner.charAt(0));
      setDisplayAllId(randomWinner);
      setWinningBet(randomWinner.charAt(0).toString());
    }

  }

  const drawRandomizerData = async () => {
    setDoneWheelSpin(false);
    setShowWinnerName(false);

    let randomInt = Math.floor(Math.random() * randomizerData.length);

    if (firstData?.length === undefined || secondData.length === undefined) {
      Swal.fire({
        icon: "error",
        title: "Select Data",
        text: "Please select data in the options before starting.",
      });

      return;
    }

    if (randomizerData?.length === 0) {
      Swal.fire({
        icon: "error",
        title: "Randomizer Data List Empty",
        text: "Please upload excel data in the options before starting.",
      });

      return;
    } else {
      do {
        if (excludedNumber.includes(randomInt)) {
          randomInt = Math.floor(Math.random() * randomizerData.length);
        }

        if (excludedNumber.length === randomizerData.length) {
          Swal.fire({
            icon: "error",
            title: "Reset",
            text: "All randomizer data has appeared. Please reset the list.",
          });

          break;
        }

        if (!excludedNumber.includes(randomInt)) {
          setExcludedNumber([...excludedNumber, randomInt]);
          dataRandomizer(randomInt);
        }
      } while (excludedNumber.includes(randomInt));
    }

    setDoneActiveSpin(false);
    SoundPlay(RouletteSound);
    setStart(true);
  };

  const showWinner = () => {
    SoundPlay(Jackpot);
    setShowWinnerName(true);
    setDoneActiveSpin(false);
    fire();
  }

  const employeeReset = async () => {
    Swal.fire({
      title: "Are you sure you want to reset?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "RESET",
    }).then((result) => {
      if (result.isConfirmed) {
        setRandomizerData(randomizerDataOriginal);
        setExcludedNumber([]);
        setRandomizerDisplay([]);
        setRandomizerUsed([]);
        setDoneActiveSpin(false);
        setShowWinnerName(false);
        setDoneWheelSpin(false);
        setDisplayFirstNumber();
        setDisplayAllId();
        setStart(false);
        setWinningBet('-1');
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Reset",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };

  const readUploadFile = async (e) => {
    e.preventDefault();

    if (e.target.files) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const data = e.target.result;
        const workbook = xlsx.read(data, { type: "array" });
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];
        const json = xlsx.utils.sheet_to_json(worksheet);

        let tempData = json;
        let dataKeyNames = Object.keys(tempData[0]);

        setArrayKeyNames(dataKeyNames);

        tempData.map((data, i) => {
          return (tempData[i].isUsed = false);
        });

        setRandomizerData(tempData);
        setRandomizerDataOriginal(tempData);

        Swal.fire({
          position: "center",
          icon: "success",
          title: "Uploaded Data",
          showConfirmButton: false,
          timer: 1500,
        });

        document.getElementById("uploadData").value = "";
      };
      reader.readAsArrayBuffer(e.target.files[0]);
    }
  };

  const SoundPlay = (src) => {
    const sound = new Howl({
      src,
      html5: true,
    });
    sound.play();
  };

  const checkboxClick = (event, type) => {
    switch (type) {
      case 1:
        setFirstData(event.target.value);
        break;
      case 2:
        setSecondData(event.target.value);
        break;
      default:
    }
  };

  const endSpin = () => {
    setDoneWheelSpin(true);
    setStart(false);
    setDoneActiveSpin(true);
    setWinningBet('-1');
  };

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? 'rgba(255, 0, 0, 0.0)' : 'rgba(255, 0, 0, 0.0)',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

  //modal
  const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const addBlurBackground = () => {
    setBackgroundBlur(backgroundBlur + 1);
  };

  const reduceBlurBackground = () => {
    if (backgroundBlur <= 0) {
      return
    } else {
      setBackgroundBlur(backgroundBlur - 1);
    }
  };

  const removeBlurBackground = () => {
    setBackgroundBlur(0);
  };

  const setDefaultBackground = () => {
    setBackgroundImage(defaultBackground);
  };

  const removeBackground = () => {
    setBackgroundImage();
  };

  useEffect(() => {
    const fileInput = document.getElementById("fileInput");
    fileInput?.addEventListener("change", (e) => {
      const file = fileInput?.files[0];
      const reader = new FileReader();
      reader?.addEventListener("load", () => {
        setBackgroundImage(reader?.result);
      });
      if (file) {
        reader?.readAsDataURL(file);
      }
      document.getElementById("fileInput").value = "";
    });
  }, [backgroundImage, randomizerDisplay, firstData, secondData, displayAllId, displayFirstNumber, doneActiveSpin]);

  return (
    <Fragment>
      <Scrollbars>
        {
          start ?
            (<Sparkle
              minSize={10}
              maxSize={30}
              flickerSpeed={"fast"}
              overflowPx={-10}
            />)
            :
            (<Sparkle
              minSize={1}
              maxSize={10}
              overflowPx={-10}
            />)
        }

        <div className="lottery-container2" style={{ backgroundImage: `url(${backgroundImage ? backgroundImage : backgroundDefault})`, filter: `blur(${backgroundBlur}px)`, WebkitFilter: `blur(${backgroundBlur}px)` }}></div>
        <div className="main-container">
          <Box sx={{ flexGrow: 1, height: '20%' }} className="">
            <Grid container>
              <Grid
                xs={12}
                display="flex"
                alignItems="center"
                justifyContent="center"
              >
                <Box
                  component="img"
                  sx={{
                    height: "90%",
                    width: "35%",
                  }}
                  alt="The house from the offer."
                  src={logoRoulette}
                />
              </Grid>
            </Grid>
          </Box>

          <Box sx={{ flexGrow: 1 }} className="">
            <Grid container>
              <Grid xs={12} className="">
                <div>
                  <RouletteWheel start={start} winningBet={winningBet} onSpinningEnd={endSpin} />
                </div>

              </Grid>
            </Grid>
          </Box>

          <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />

          <Box sx={{ flexGrow: 1, height: '40%' }}>
            <Grid container>
              <Grid
                xs={12}
                display="flex"
                alignItems="center"
                justifyContent="center"
                className="button-box"
                paddingTop={2}
              >
                {doneActiveSpin === true ? (
                  <Fragment>
                    <Button className="spin-button" variant="contained" onClick={() => showWinner()} color="error" size="small">SHOW WINNER</Button>
                  </Fragment>
                ) : (
                  <Fragment>
                    <Button className="spin-button" variant="contained" disabled={start} color="warning" onClick={() => drawRandomizerData()} size="small">SPIN</Button>
                  </Fragment>
                )}
              </Grid>
              <Grid
                xs={12}
                className={doneWheelSpin === true ? 'display-container' : ""}
                display="flex"
                alignItems="center"
                justifyContent="center"
                paddingTop={2}
              >
                <Animate play start={{ opacity: 0 }} end={{ opacity: 1 }}>
                  {doneWheelSpin === true && showWinnerName === true ? (
                    <Fragment>
                      <span className="display-number">
                        {displayAllId}{" "}
                      </span>
                    </Fragment>
                  ) : (
                    <></>
                  )}

                  {doneWheelSpin === true && showWinnerName === false ? (
                    <Fragment>
                      <span className="display-number display-first-number">
                        {displayFirstNumber}{" "}
                      </span>
                    </Fragment>
                  ) : (
                    <></>
                  )}
                </Animate>
              </Grid>
              <Grid
                xs={12}
                className={doneWheelSpin === true ? 'display-container' : ""}
                display="flex"
                alignItems="center"
                justifyContent="center"
              >
                {showWinnerName === true ? (
                  <Fragment>
                    <Animate play start={{ opacity: 0 }} end={{ opacity: 1 }}>
                      <span className="display-name">
                        {randomizerDisplay[secondData]}{" "}
                      </span>
                    </Animate>
                  </Fragment>
                ) : (
                  <></>
                )}
              </Grid>
            </Grid>
          </Box>
        </div>

        <div>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>Options</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                  <Grid xs={12}>
                    <Item>
                      <form>
                        <label
                          htmlFor="uploadData"
                          style={{
                            display: "block",
                            cursor: "pointer",
                            background: "black",
                            padding: "10px",
                            color: "white",
                            textAlign: "center"
                          }}
                        >
                          Upload Form Data
                        </label>
                        <input
                          className="d-none"
                          style={{ visibility: "hidden" }}
                          type="file"
                          name="uploadData"
                          id="uploadData"
                          onChange={readUploadFile}
                        />
                      </form>
                    </Item>
                  </Grid>

                  <Grid xs={12}>
                    <Item>
                      {arrayKeyNames?.length !== 0 ? (
                        <FormControl>
                          <FormLabel id="demo-row-radio-buttons-group-label1">First Data</FormLabel>
                          <RadioGroup
                            row
                            aria-labelledby="demo-row-radio-buttons-group-label1"
                            name="row-radio-buttons-group1"
                            value={firstData}
                            onChange={(e) => checkboxClick(e, 1)}
                          >
                            {arrayKeyNames?.map((data) => {
                              return (
                                <FormControlLabel
                                  value={data}
                                  control={<Radio />}
                                  label={data}

                                  key={data + "1"} />
                              );
                            })}
                          </RadioGroup>

                          <FormLabel id="demo-row-radio-buttons-group-label2">Second Data</FormLabel>
                          <RadioGroup
                            row
                            aria-labelledby="demo-row-radio-buttons-group-label2"
                            name="row-radio-buttons-group2"
                            value={secondData}
                            onChange={(e) => checkboxClick(e, 2)}
                          >
                            {arrayKeyNames?.map((data) => {
                              return (
                                <FormControlLabel
                                  value={data}
                                  control={<Radio />}
                                  label={data}

                                  key={data + "2"} />
                              );
                            })}
                          </RadioGroup>
                        </FormControl>
                      ) : (
                        <></>
                      )}
                    </Item>
                  </Grid>

                  <Grid xs={12}>
                    <Item>
                      <form>
                        <label
                          htmlFor="fileInput"
                          style={{
                            display: "block",
                            cursor: "pointer",
                            background: "black",
                            padding: "10px",
                            color: "white",
                            textAlign: "center"
                          }}
                        >
                          Set Background Image
                        </label>
                        <input
                          className="d-none"
                          style={{ visibility: "hidden" }}
                          type="file"
                          name="fileInput"
                          id="fileInput"
                        />
                      </form>
                    </Item>
                  </Grid>

                  <Grid xs={3}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => setDefaultBackground()}>
                          Add Default Background
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={3}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => removeBackground()}>
                          Remove Background
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={2}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => addBlurBackground()}>
                          Add Blur
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={2}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => reduceBlurBackground()}>
                          Reduce Blur
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={2}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => removeBlurBackground()}>
                          Remove Blur
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={12}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={handleClickOpen}>
                          Open Winner List
                        </Button>
                        <Dialog
                          fullScreen
                          open={open}
                          onClose={handleClose}
                          TransitionComponent={Transition}
                        >
                          <AppBar sx={{ position: 'relative' }}>
                            <Toolbar>
                              <IconButton
                                edge="start"
                                color="inherit"
                                onClick={handleClose}
                                aria-label="close"
                              >
                                <CloseIcon />
                              </IconButton>
                              <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                                Winner List
                              </Typography>
                              <Button autoFocus color="inherit" onClick={handleClose}>
                                close
                              </Button>
                            </Toolbar>
                          </AppBar>
                          <List>
                            {randomizerUsed.length > 0 ? (
                              randomizerUsed?.map((data, i) => (
                                <Fragment key={i}>
                                  <ListItem >
                                    <ListItemText
                                      primary={`${i + 1} - ${data[firstData]}`}
                                      secondary={`${data[secondData]}`}
                                    />
                                  </ListItem>
                                </Fragment>
                              ))
                            ) : (
                              <></>
                            )}
                          </List>
                        </Dialog>
                      </div>
                    </Item>
                  </Grid>

                  <Grid xs={12}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={employeeReset} id="reset-button">
                          RESET
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                </Grid>
              </Box>

            </AccordionDetails>
          </Accordion>
        </div>
      </Scrollbars>


    </Fragment >
  );
}

export default App;